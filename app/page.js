import { PrismaClient } from "@prisma/client";
import AddTodo from "@/components/shared/AddTodo";
import React from "react";
import Todo from "@/components/shared/Todo";
import './styles.css';

const prisma = new PrismaClient();

async function getData(filter) { //Parte da Amanda Para a Lista de Tarefas
  let whereClause = {};

  if (filter === 'active') {
    whereClause.isCompleted = false;
  } else if (filter === 'completed') {
    whereClause.isCompleted = true;
  }

  const data = await prisma.todo.findMany({
    where: whereClause,
    select: {
      title: true,
      id: true,
      isCompleted: true,
    },
    orderBy: {
      createdAt: "desc",
    },
  });

  return data;
}

const Home = async () => {
  const activeTodos = await getData('active');
  const completedTodos = await getData('completed');

  return (
    <div className="w-screen py-20 flex justify-center flex-col items-center">
      <span className="text-3xl font-extrabold uppercase">
        To-do-app <span className="text-red-600 ml-2">Server Actions</span>
      </span>
      <br></br>
      <div className="flex justify-center flex-col items-center w-[1000px]">
        <AddTodo />
        <div className="flex flex-col gap-5 items-center justify-center mt-10 w-full">
          <h2>Tarefas Ativas:</h2>
          {activeTodos.map((todo, index) => (
            <div className={`w-full task-active`} key={index}>
              <Todo todo={todo} />
            </div>
          ))}
          <h2>Tarefas Completas:</h2>
          {completedTodos.map((todo, index) => (
            <div className={`w-full task-completed`} key={index}>
              <Todo todo={todo} />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Home;
