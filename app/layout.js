import "./globals.css";


export const metadata = {
  title: "FC Solutions Todo App",
  description: "fc-solutions-to-do-app",
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body className="bg-zinc-400">{children}</body>
    </html>
  );
}
